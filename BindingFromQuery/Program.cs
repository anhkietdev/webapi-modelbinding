//var builder = WebApplication.CreateBuilder(args);
//var app  = builder.Build();

//app.MapGet("/query", (int a) =>
//{
//    return $"intput into query a = {a}";
//});

//app.Run();


var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

app.MapGet("/query", (int a, int b, int c) =>
{
    return $"Sum of {a} and {b} is {c}";
});

app.Run();


