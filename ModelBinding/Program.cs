//var builder = WebApplication.CreateBuilder();
//var app = builder.Build();

//app.MapPost("/customer", (Customer customer) => customer);
//app.Run();


var builder = WebApplication.CreateBuilder();
var app = builder.Build();
app.MapPost("/post", () => "post");

app.MapGet("/get", () => "get");

app.MapDelete("/delete", () => "delete");

app.MapPut("/put", () => "put");

app.MapPatch("/patch", () => "patch");

app.Run();


class Customer
{
    public int CustomerID { get; set; }
    public string CustomerName { get; set; }
    public string PhoneNumber { get; set; }
    public string Address { get; set; }
}
