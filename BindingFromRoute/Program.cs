//Binding with route
//var builder = WebApplication.CreateBuilder();
//var app = builder.Build();

//app.MapGet("/route/{id}", (int id) => {

//    return $"id of this route = {id}";
//});

//app.Run();


//Multiple route
var builder = WebApplication.CreateBuilder();
var app = builder.Build();

app.MapGet("/route/{id}/{name}", (int id, string name) => {

    return $"id of this route = {id} and belong to {name}";
});

app.Run();



